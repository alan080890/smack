//
//  ChatVC.swift
//  Smack
//
//  Created by Alan Ramos on 10/9/18.
//  Copyright © 2018 Alan Ramos. All rights reserved.
//

import UIKit

class ChatVC: UIViewController {
    
    //Outlet
    @IBOutlet weak var menuBtn: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        menuBtn.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
    }

}
